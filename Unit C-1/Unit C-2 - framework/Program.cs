﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit_C_2___framework
{
// Creator: Paul Cameron
// Class: CSCI-1630-SP2021
// Date: 02/04/2021
// Homework: Unit C, part A/B (BMI and Karvonen Formula)

/// <summary>
///     This application takes an input of the user's height, weight, age and heart rate. 
///     First stage, uses the height and weight to caluclate their BMI.
///     Second stage, uses the age and heart rate, and Karvonen Formula to see their maximium heart rate.
/// </summary> 

    class Program
    {
        static void Main(string[] args)
        {
            Program kf = new Program(); // declare the method to call KarvonenFormula() 

            // Greeting message
            Console.WriteLine(" *** BMI and Karvonen Calculator ***\n Please enter the following values for the user . . .\n");

            Console.Write("What is your height in inches: "); // Input user's height
            double heightInches = Convert.ToDouble(Console.ReadLine());
            
            Console.Write("What is your weight in pounds: "); // Input user's weight
            double weightPounds = Convert.ToDouble(Console.ReadLine());

            Console.Write("What is your age: "); // Input user's age
            int userAge = Convert.ToInt32(Console.ReadLine());

            Console.Write("What is your heart rate: "); // Input user's resting heart rate
            int userHeartRate = Convert.ToInt32(Console.ReadLine());   

            Console.WriteLine("\nResults . . .\n");
            var bmi = BMIFormula(heightInches, weightPounds); // categorize the user's BMI level
            Console.WriteLine("Your BMI is: {0:0.00} -- {1}", bmi.Item1, bmi.Item2 );

            Console.WriteLine("\nExercise Intensity Heart Rates:"); //print the next excercise
            Console.WriteLine("Intensity    Max Heart Rate \n"); 
            
            // loop and calculate the maximum heart rate starting at 50%, increasing every 5% until it hits 95%
            for (int i = 50; i <= 95; i++) // intensity range is [50:95]
            {
                int mhr = kf.KarvonenFormula(userAge, userHeartRate, i);
                Console.WriteLine("{0:#\\%}    --   {1}", i, mhr);
                i += 4; // move intensity up 5%
            }

            // Pause the program and wait
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadLine();
        }
        public int KarvonenFormula(int userAge, int userHeartRate, int intensity)
        { 
            return (((220 - userAge) - userHeartRate) * intensity / 100) + userHeartRate;;
        }

        public static Tuple <double, string> BMIFormula(double height, double weight)
        {
            // declare constants for BMI
            double underWeight = 18.5;
            double normalWeight = 24.9;
            double overWeight = 29.9;
            string message = "";

            // Calulate the inches to meters and pounds to kilograms
            double bmi = (weight / 2.205) / Math.Pow((height / 39.37), 2);

            
            if (underWeight >= bmi)// Find the user's BMI category
            {
                message = "Underweight";
            }
            else if (underWeight < bmi && bmi <= normalWeight)
            {
                message = "Normal weight";
            }
            else if (normalWeight < bmi && bmi <= overWeight)
            {
                message = "Overweight";
            }
            else
            {
                message = "Obesity";
            }
            return Tuple.Create(bmi, message);
        }
    }
}
