﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UnitFHomework
{
// Creator: Paul Cameron
// Class: CSCI-1630-SP2021
// Date: 02/04/2021
// Homework: Unit F, uses forms to recieve input and calculates a payment plan
/// <summary>
///     This application takes multiple inputs in .NET's design and calculates a payment plan for the user.
/// </summary> 

    public partial class Form1 : Form
    {
        public Form1()
        {

            InitializeComponent();
        }

        // Event -- Program begins when the use clicks "Calculate 
        private void button1_Click(object sender, EventArgs e)
        {
            // Values that are recieving input
            double principle = 0;
            double downPayment = 0;
            double interestRate = 0;
            int loanTerm = 0;

            // Error messages
            string emptyMessage = "A value needs to be entered for {0}";
            string nonNumericMessage = "You've entered a non-numeric for {0} or a value not greater than zero.";
            string nonNumericMessage_one = "You've entered a non-numeric for {0} or a value less than zero.";
            string stringPrice = "Purchase Price";
            string stringDownPayment = "Down Payment";
            string stringInterestRate = "Interest Rate";
            string stringLoanTerm = "Loan Term";

            // Change the background color of the fields to the default color in case the validation passes.
            textBoxPurchasePrice.BackColor = SystemColors.Window;
            textBoxDownPayment.BackColor = SystemColors.Window;
            textBoxInterestRate.BackColor = SystemColors.Window;
            textBoxLoanTerm.BackColor = SystemColors.Window;


            // Purchase price validation
            if (string.IsNullOrEmpty(textBoxPurchasePrice.Text))
            {
                textBoxPurchasePrice.BackColor = Color.Tomato;
                MessageBox.Show(string.Format(emptyMessage, stringPrice));
            }
            else if (!double.TryParse(textBoxPurchasePrice.Text, out principle) || principle <= 0)
            {
                textBoxPurchasePrice.BackColor = Color.Tomato;
                MessageBox.Show(string.Format(nonNumericMessage, stringPrice));
            }


            // Downpayment validation
            else if (string.IsNullOrEmpty(textBoxDownPayment.Text))
            {
                textBoxDownPayment.BackColor = Color.Tomato;

                MessageBox.Show(string.Format(emptyMessage, stringDownPayment));
            }
            else if (!double.TryParse(textBoxDownPayment.Text, out downPayment) || downPayment < 0)
            {
                textBoxDownPayment.BackColor = Color.Tomato;

                MessageBox.Show(string.Format(nonNumericMessage_one, stringDownPayment));
            }


            // Interest Rate validation
            else if (string.IsNullOrEmpty(textBoxInterestRate.Text))
            {
                textBoxInterestRate.BackColor = Color.Tomato;

                MessageBox.Show(string.Format(emptyMessage, stringInterestRate));
            }
            else if (!double.TryParse(textBoxInterestRate.Text, out interestRate) || interestRate < 0)
            {
                textBoxInterestRate.BackColor = Color.Tomato;

                MessageBox.Show(string.Format(nonNumericMessage_one, stringInterestRate));
            }

              // Loan Term validation
            else if (string.IsNullOrEmpty(textBoxLoanTerm.Text))
            {
                textBoxLoanTerm.BackColor = Color.Tomato;

                MessageBox.Show(string.Format(emptyMessage, stringLoanTerm));
            }
            else if(!int.TryParse(textBoxLoanTerm.Text, out loanTerm) || loanTerm <= 0)
            {
                textBoxLoanTerm.BackColor = Color.Tomato;

                MessageBox.Show(string.Format(nonNumericMessage, stringLoanTerm));
            }

            // Passed validation and continuing on to calculations
            else
            {
                double monthlyPaymentAmount = paymentFormula(principle, interestRate, loanTerm);

                textBoxAmountFinance.Text = String.Format($"{principle-downPayment:c}");
                textBoxMonthlyPayment.Text = String.Format($"{monthlyPaymentAmount:c}");
            }           
        }

        // static method that calculates the payment
        public static double paymentFormula(double principle, double interest, int loanTerm)
        {
            //If interest is zero
            if (interest == 0)
            {
                return (principle /  loanTerm);
            }
            // calculate the monthly balance
            else
            {
            double monthlyRate = interest / 100 / 12;

            return (monthlyRate * principle * ((Math.Pow(1 + monthlyRate, loanTerm)/(Math.Pow(1+ monthlyRate, loanTerm)-1))));
            }
        }

        // Event -- closes the program when "closed" button is clicked.
        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
