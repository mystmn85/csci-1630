﻿
namespace UnitFHomework
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.labelPurchasePrice = new System.Windows.Forms.Label();
            this.labelDownPayment = new System.Windows.Forms.Label();
            this.labelInterestRate = new System.Windows.Forms.Label();
            this.labelLoanTerm = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPurchasePrice = new System.Windows.Forms.TextBox();
            this.textBoxDownPayment = new System.Windows.Forms.TextBox();
            this.textBoxInterestRate = new System.Windows.Forms.TextBox();
            this.textBoxLoanTerm = new System.Windows.Forms.TextBox();
            this.textBoxAmountFinance = new System.Windows.Forms.TextBox();
            this.textBoxMonthlyPayment = new System.Windows.Forms.TextBox();
            this.labelMonthlyPayment = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Location = new System.Drawing.Point(192, 248);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(75, 23);
            this.buttonCalculate.TabIndex = 0;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(441, 248);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // labelPurchasePrice
            // 
            this.labelPurchasePrice.AutoSize = true;
            this.labelPurchasePrice.Location = new System.Drawing.Point(108, 54);
            this.labelPurchasePrice.Name = "labelPurchasePrice";
            this.labelPurchasePrice.Size = new System.Drawing.Size(82, 13);
            this.labelPurchasePrice.TabIndex = 2;
            this.labelPurchasePrice.Text = "Purchase Price:";
            this.labelPurchasePrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDownPayment
            // 
            this.labelDownPayment.AutoSize = true;
            this.labelDownPayment.Location = new System.Drawing.Point(69, 85);
            this.labelDownPayment.Name = "labelDownPayment";
            this.labelDownPayment.Size = new System.Drawing.Size(121, 13);
            this.labelDownPayment.TabIndex = 3;
            this.labelDownPayment.Text = "Down Payment Amount:";
            this.labelDownPayment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelInterestRate
            // 
            this.labelInterestRate.AutoSize = true;
            this.labelInterestRate.Location = new System.Drawing.Point(77, 118);
            this.labelInterestRate.Name = "labelInterestRate";
            this.labelInterestRate.Size = new System.Drawing.Size(112, 13);
            this.labelInterestRate.TabIndex = 4;
            this.labelInterestRate.Text = "Interest Rate (annual):";
            this.labelInterestRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelLoanTerm
            // 
            this.labelLoanTerm.AutoSize = true;
            this.labelLoanTerm.Location = new System.Drawing.Point(84, 153);
            this.labelLoanTerm.Name = "labelLoanTerm";
            this.labelLoanTerm.Size = new System.Drawing.Size(106, 13);
            this.labelLoanTerm.TabIndex = 5;
            this.labelLoanTerm.Text = "Loan Term (monthly):";
            this.labelLoanTerm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(336, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Amount to Finance:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxPurchasePrice
            // 
            this.textBoxPurchasePrice.Location = new System.Drawing.Point(192, 47);
            this.textBoxPurchasePrice.Name = "textBoxPurchasePrice";
            this.textBoxPurchasePrice.Size = new System.Drawing.Size(100, 20);
            this.textBoxPurchasePrice.TabIndex = 7;
            // 
            // textBoxDownPayment
            // 
            this.textBoxDownPayment.Location = new System.Drawing.Point(192, 78);
            this.textBoxDownPayment.Name = "textBoxDownPayment";
            this.textBoxDownPayment.Size = new System.Drawing.Size(100, 20);
            this.textBoxDownPayment.TabIndex = 9;
            // 
            // textBoxInterestRate
            // 
            this.textBoxInterestRate.Location = new System.Drawing.Point(192, 111);
            this.textBoxInterestRate.Name = "textBoxInterestRate";
            this.textBoxInterestRate.Size = new System.Drawing.Size(100, 20);
            this.textBoxInterestRate.TabIndex = 11;
            // 
            // textBoxLoanTerm
            // 
            this.textBoxLoanTerm.Location = new System.Drawing.Point(192, 146);
            this.textBoxLoanTerm.Name = "textBoxLoanTerm";
            this.textBoxLoanTerm.Size = new System.Drawing.Size(100, 20);
            this.textBoxLoanTerm.TabIndex = 13;
            // 
            // textBoxAmountFinance
            // 
            this.textBoxAmountFinance.Location = new System.Drawing.Point(441, 47);
            this.textBoxAmountFinance.Name = "textBoxAmountFinance";
            this.textBoxAmountFinance.ReadOnly = true;
            this.textBoxAmountFinance.Size = new System.Drawing.Size(100, 20);
            this.textBoxAmountFinance.TabIndex = 15;
            // 
            // textBoxMonthlyPayment
            // 
            this.textBoxMonthlyPayment.Location = new System.Drawing.Point(441, 78);
            this.textBoxMonthlyPayment.Name = "textBoxMonthlyPayment";
            this.textBoxMonthlyPayment.ReadOnly = true;
            this.textBoxMonthlyPayment.Size = new System.Drawing.Size(100, 20);
            this.textBoxMonthlyPayment.TabIndex = 17;
            // 
            // labelMonthlyPayment
            // 
            this.labelMonthlyPayment.AutoSize = true;
            this.labelMonthlyPayment.Location = new System.Drawing.Point(344, 85);
            this.labelMonthlyPayment.Name = "labelMonthlyPayment";
            this.labelMonthlyPayment.Size = new System.Drawing.Size(91, 13);
            this.labelMonthlyPayment.TabIndex = 16;
            this.labelMonthlyPayment.Text = "Monthly Payment:";
            this.labelMonthlyPayment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 353);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Created by: Paul Cameron";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 392);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxMonthlyPayment);
            this.Controls.Add(this.labelMonthlyPayment);
            this.Controls.Add(this.textBoxAmountFinance);
            this.Controls.Add(this.textBoxLoanTerm);
            this.Controls.Add(this.textBoxInterestRate);
            this.Controls.Add(this.textBoxDownPayment);
            this.Controls.Add(this.textBoxPurchasePrice);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelLoanTerm);
            this.Controls.Add(this.labelInterestRate);
            this.Controls.Add(this.labelDownPayment);
            this.Controls.Add(this.labelPurchasePrice);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.buttonCalculate);
            this.Name = "Form1";
            this.Text = "Monthly Payment Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label labelPurchasePrice;
        private System.Windows.Forms.Label labelDownPayment;
        private System.Windows.Forms.Label labelInterestRate;
        private System.Windows.Forms.Label labelLoanTerm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPurchasePrice;
        private System.Windows.Forms.TextBox textBoxDownPayment;
        private System.Windows.Forms.TextBox textBoxInterestRate;
        private System.Windows.Forms.TextBox textBoxLoanTerm;
        private System.Windows.Forms.TextBox textBoxAmountFinance;
        private System.Windows.Forms.TextBox textBoxMonthlyPayment;
        private System.Windows.Forms.Label labelMonthlyPayment;
        private System.Windows.Forms.Label label2;
    }
}

