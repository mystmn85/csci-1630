﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Creator: Paul Cameron
// Class: CSCI-1630-SP2021
// Date: 02/11/2021
// Homework: Unit D, part A/B (
//      - part A: confirm conversion of string to int and is greater than a million
//      - part B: create a loop that calculates pi using the Leibniz Formula. 
// )

/// <summary>
///     This application takes an input, confirms it as int and is greater than a million. 
///     Otherwise, it'll loop and keep asking until the conditions are satisfied.
///     
///     Part B: Calculates pi based on the Leibniz Formula with the index of 10, 
///     1000, 100000, 500000, 1000000, and the value of part A's input (which is greater than a million).
///     Printing the results on new lines.
/// </summary> 


namespace Unit_D
{
    class Program
    {
        // following this link to calcute PI in C#
        // https://stackoverflow.com/questions/39395/how-do-i-calculate-pi-in-c
        public static string LeibnizFormula(int runtime)
        {
            double formulaResults = 0;
            
            for (int i=0; i < runtime; i++)
            {
                // To help get the formula correct I used...
                // https://www.codeproject.com/Articles/813185/Calculating-the-Number-PI-Through-Infinite-Sequenc
                formulaResults += (4 * Math.Pow(-1, i)) / (2 * i + 1);
            }

            // I wasn't sure how to use the "return" feature in order to create my list
            // so I did a Console.Write into the List. 
            //
            // Removing "return" threw an error so it returns nothing.
            // In Java they use GET and SET for such features

            Console.Write("At {0:#,##0} iterations, the value of pi is {1:N10}\n", runtime, formulaResults);

            return "";
        }

        static void Main(string[] args)
        {
            // Later we'll loop a string of messages with diff variables and add it to a List.
            List<string> message = new List<string>();
            int userNumber = 0;
            bool confirmation = false;
           
            // Start my application greeting.
            Console.WriteLine("Paul's Implementation of the Leibniz Formula: \n");

            // Ask the user a question.
            Console.WriteLine("Please enter the number of times to run this calculation...");
            
            // Keep asking the question until conditions are met.
            while (!confirmation) {
                Console.Write("Please enter the value > 1 million: ");
                int.TryParse(Console.ReadLine(), out userNumber);

                // Ask the user for a value greater than a million and confirm the input is int.
                if (userNumber > 1000000)
                {
                    confirmation = true;
                }
            }
            // Create spacing for cmd
            Console.WriteLine("\n\n\n");

            message.Add(LeibnizFormula(10)); // 10 interations
            message.Add(LeibnizFormula(1000)); //100,000 interations
            message.Add(LeibnizFormula(100000)); //100,000 interations
            message.Add(LeibnizFormula(500000)); //500,000 interations
            message.Add(LeibnizFormula(1000000)); // 1,000,000 interations
            message.Add(LeibnizFormula(userNumber)); //user input greater than a million interations

            // show results from the Leibniz formula on new lines
            foreach ( string eachLine in message)
            {
                Console.Write("{0}",eachLine);
            }

            // Pause the program and wait for the user to end
            Console.WriteLine("\nPress any key to end...");
            Console.ReadLine();
        } 
    }
}
