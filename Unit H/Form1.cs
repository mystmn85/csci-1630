﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonSubmission_Click(object sender, EventArgs e)
        {
            textBoxStudentComments.Text = "Notes: I wasn't able to figure out how to maintain the spacing" +
                " while cleaning the array from puncuations and symbols. " +
                "Then to keep the positioning as it goes into the 2d array. This excercise was very " +
                "challenging or maybe I was overthinking the whole process.It was very fun though.  ";

            // Check the inputs for null or empty values
           
            if (String.IsNullOrEmpty(textBoxSaltedPhrase.Text) || String.IsNullOrWhiteSpace(textBoxSaltedPhrase.Text))
            {
                MessageBox.Show("Enter your secret key");
            }
            else if (String.IsNullOrEmpty(textBoxHideMeText.Text) || String.IsNullOrWhiteSpace(textBoxHideMeText.Text))
            {
                MessageBox.Show("Enter a phrase to be translated..");
            }
            else
            {

                /*
                 * Take the Secret word and remove CHAR symbols and numbers, leaving only letters.
                 */
                HashSet<string> cleanSaltedHash = cleanedSecretPhrase(textBoxSaltedPhrase.Text);

                /*
                 * Take the secret word and merge it into a 25.length HASH SET (HASH SET was chosen to not allow duplicate letters).
                 */
                HashSet<string> completedString = combineAndOrderGrid(cleanSaltedHash);

                /*
                 * With the newly created 25 length HASH, we transform it into a 2D array - 5x5 grid
                 */
                string[,] finalGrid = hashedGrid(completedString);

                /*
                 * Now we are ready to take the plain text input from the user and encrypt it based on the 5x5 grid.
                 * I/J are represented as the same character.
                 * Compare each letter in the sentence to the grid.
                 */
                List<string> mergeHashGrid = new List<string>();

                List<string> filteredPlainText = cleanPlainText(textBoxHideMeText.Text);

                foreach (string eachLetter in filteredPlainText)
                {
                    // The grid is 5x5, I(s) and J(s) are the same value
                    string cleanedEachLetter = eachLetter.Equals("i") ? "j" : eachLetter;

                    for (int i = 0; i < 5; i++)
                    {
                        for (int j = 0; j < 5; j++)
                        {
                            /*
                             * If the plain text is found on the hash grid, invert the [x,y] positioning to [y,x]
                             * then add the new position to a new LIST
                             */
                            if (cleanedEachLetter.Equals(finalGrid[i, j]))
                            {
                                mergeHashGrid.Add(finalGrid[j, i]);
                            }
                        }
                    }

                }
                // Take the List of encrypted CHARs and make it one string, then output to the field.
                textBoxOutput.Text = String.Join("", mergeHashGrid).ToUpper();

                /*
                 * This section was used for testing purposes.
                 */

                /*
                List<string> p = new List<string>()
                foreach(string x in completedString)
                {
                    p.Add(x);
                }

                textBoxOutput.Text = String.Join("", p).ToUpper();
               */
            }
        }
        public static List<string> cleanPlainText(string plainText)
        {
            // Clean the plain text for punctuations and numbers 

            List<string> CleanedPlainText = new List<string>();
            foreach (char letter in plainText)
            {

                if (Char.IsLetter(letter))
                {
                    CleanedPlainText.Add(letter.ToString().ToLower());
                }
            }
            return CleanedPlainText;
        }

        public static HashSet<string> cleanedSecretPhrase(string saltedPhrase)
        {
            /* 
             * Remove any duplicate letters from the secret word by making this a Hashset. 
             * Also, remove puncuations and numbers from the phrase
            */

            HashSet<string> uniqueList = new HashSet<string>();
            foreach (char letter in saltedPhrase)
            {

                if (Char.IsLetter(letter))
                {
                    uniqueList.Add(letter.ToString().ToLower());
                }
            }
            return uniqueList;
        }

        public static HashSet<string> combineAndOrderGrid(HashSet<string> saltedPhrase)
        {
            // Add the rest of the alphabet to our hash. We used a Hash set because every char is unique.
            string[] alphahet = {"a", "b", "c", "d","e","f","g","h","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z" };

            foreach(string alpha in alphahet)
            {
                saltedPhrase.Add(alpha);
            }
            return saltedPhrase;
        }

        public static string[,] hashedGrid(HashSet<string> hashedAlphabet)
        {
            /*
             * Initiate our 5x5 grid of rows and columns
             */
            string[,] gridStyle = new string[5, 5];

            // Find the values of our hashed alphabet and place them in the 5x5 grid.  
            int h = 0;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    /*
                     * Had to learn what ElementAt did in order to index a Hash String
                     * LINK:: https://stackoverflow.com/questions/23596490/cannot-apply-indexing-with-to-an-expression-of-type-system-array-with-c-sha/23596604
                     */
                    gridStyle[i, j] = hashedAlphabet.ElementAt(h);
                    h++;
                }
            }
            return gridStyle;
        }

        private void buttonClearForm_Click(object sender, EventArgs e)
        {
            // Used to clear the user fields.
            textBoxSaltedPhrase.Text = String.Empty;
            textBoxHideMeText.Text = String.Empty;
        }
    }
}
