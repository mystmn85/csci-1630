﻿
namespace Exercise5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSecretWord = new System.Windows.Forms.Label();
            this.labelCryptMe = new System.Windows.Forms.Label();
            this.textBoxSaltedPhrase = new System.Windows.Forms.TextBox();
            this.textBoxHideMeText = new System.Windows.Forms.TextBox();
            this.buttonSubmitForm = new System.Windows.Forms.Button();
            this.buttonExitForm = new System.Windows.Forms.Button();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.buttonClearForm = new System.Windows.Forms.Button();
            this.labelOutputText = new System.Windows.Forms.Label();
            this.labelStudentName = new System.Windows.Forms.Label();
            this.textBoxStudentComments = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelSecretWord
            // 
            this.labelSecretWord.AutoSize = true;
            this.labelSecretWord.Location = new System.Drawing.Point(46, 39);
            this.labelSecretWord.Name = "labelSecretWord";
            this.labelSecretWord.Size = new System.Drawing.Size(67, 13);
            this.labelSecretWord.TabIndex = 0;
            this.labelSecretWord.Text = "Salt my input";
            // 
            // labelCryptMe
            // 
            this.labelCryptMe.AutoSize = true;
            this.labelCryptMe.Location = new System.Drawing.Point(46, 119);
            this.labelCryptMe.Name = "labelCryptMe";
            this.labelCryptMe.Size = new System.Drawing.Size(55, 13);
            this.labelCryptMe.TabIndex = 1;
            this.labelCryptMe.Text = "Input Text";
            // 
            // textBoxSaltedPhrase
            // 
            this.textBoxSaltedPhrase.Location = new System.Drawing.Point(49, 55);
            this.textBoxSaltedPhrase.Name = "textBoxSaltedPhrase";
            this.textBoxSaltedPhrase.Size = new System.Drawing.Size(379, 20);
            this.textBoxSaltedPhrase.TabIndex = 0;
            // 
            // textBoxHideMeText
            // 
            this.textBoxHideMeText.Location = new System.Drawing.Point(49, 135);
            this.textBoxHideMeText.Multiline = true;
            this.textBoxHideMeText.Name = "textBoxHideMeText";
            this.textBoxHideMeText.Size = new System.Drawing.Size(379, 65);
            this.textBoxHideMeText.TabIndex = 1;
            // 
            // buttonSubmitForm
            // 
            this.buttonSubmitForm.Location = new System.Drawing.Point(49, 321);
            this.buttonSubmitForm.Name = "buttonSubmitForm";
            this.buttonSubmitForm.Size = new System.Drawing.Size(104, 23);
            this.buttonSubmitForm.TabIndex = 2;
            this.buttonSubmitForm.Text = "Translate Text";
            this.buttonSubmitForm.UseVisualStyleBackColor = true;
            this.buttonSubmitForm.Click += new System.EventHandler(this.buttonSubmission_Click);
            // 
            // buttonExitForm
            // 
            this.buttonExitForm.Location = new System.Drawing.Point(524, 321);
            this.buttonExitForm.Name = "buttonExitForm";
            this.buttonExitForm.Size = new System.Drawing.Size(75, 23);
            this.buttonExitForm.TabIndex = 3;
            this.buttonExitForm.Text = "Exit";
            this.buttonExitForm.UseVisualStyleBackColor = true;
            this.buttonExitForm.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Location = new System.Drawing.Point(49, 227);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ReadOnly = true;
            this.textBoxOutput.Size = new System.Drawing.Size(379, 75);
            this.textBoxOutput.TabIndex = 4;
            this.textBoxOutput.TabStop = false;
            // 
            // buttonClearForm
            // 
            this.buttonClearForm.Location = new System.Drawing.Point(159, 321);
            this.buttonClearForm.Name = "buttonClearForm";
            this.buttonClearForm.Size = new System.Drawing.Size(75, 23);
            this.buttonClearForm.TabIndex = 5;
            this.buttonClearForm.Text = "Clear";
            this.buttonClearForm.UseVisualStyleBackColor = true;
            this.buttonClearForm.Click += new System.EventHandler(this.buttonClearForm_Click);
            // 
            // labelOutputText
            // 
            this.labelOutputText.AutoSize = true;
            this.labelOutputText.Location = new System.Drawing.Point(46, 211);
            this.labelOutputText.Name = "labelOutputText";
            this.labelOutputText.Size = new System.Drawing.Size(63, 13);
            this.labelOutputText.TabIndex = 6;
            this.labelOutputText.Text = "Output Text";
            // 
            // labelStudentName
            // 
            this.labelStudentName.AutoSize = true;
            this.labelStudentName.Location = new System.Drawing.Point(648, 9);
            this.labelStudentName.Name = "labelStudentName";
            this.labelStudentName.Size = new System.Drawing.Size(140, 13);
            this.labelStudentName.TabIndex = 7;
            this.labelStudentName.Text = "Paul Cameron - 03/17/2021";
            // 
            // textBoxStudentComments
            // 
            this.textBoxStudentComments.Location = new System.Drawing.Point(49, 363);
            this.textBoxStudentComments.Multiline = true;
            this.textBoxStudentComments.Name = "textBoxStudentComments";
            this.textBoxStudentComments.ReadOnly = true;
            this.textBoxStudentComments.Size = new System.Drawing.Size(379, 75);
            this.textBoxStudentComments.TabIndex = 8;
            this.textBoxStudentComments.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBoxStudentComments);
            this.Controls.Add(this.labelStudentName);
            this.Controls.Add(this.labelOutputText);
            this.Controls.Add(this.buttonClearForm);
            this.Controls.Add(this.textBoxOutput);
            this.Controls.Add(this.buttonExitForm);
            this.Controls.Add(this.buttonSubmitForm);
            this.Controls.Add(this.textBoxHideMeText);
            this.Controls.Add(this.textBoxSaltedPhrase);
            this.Controls.Add(this.labelCryptMe);
            this.Controls.Add(this.labelSecretWord);
            this.Name = "Form1";
            this.Text = "Exercise5";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSecretWord;
        private System.Windows.Forms.Label labelCryptMe;
        private System.Windows.Forms.TextBox textBoxSaltedPhrase;
        private System.Windows.Forms.TextBox textBoxHideMeText;
        private System.Windows.Forms.Button buttonSubmitForm;
        private System.Windows.Forms.Button buttonExitForm;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.Button buttonClearForm;
        private System.Windows.Forms.Label labelOutputText;
        private System.Windows.Forms.Label labelStudentName;
        private System.Windows.Forms.TextBox textBoxStudentComments;
    }
}

