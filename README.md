Spring of 2021 CSCI-1630: C# Programming I
====================================

This class is on going and will be updated weekly. 

Week 1 / Unit A
-------
Inputs the height and width of a floor to install and calculates a cost

Week 2 / Unit B
-------
No labs this week

Week 3 / Unit C
-------
Input the user's height, weight, age, and heart rate. First, calculates the BMI. Second part uses the Karvonen Formula. Based on the methood results
the program will print a summary of both.

Week 4 / Unit D
-------
Part A: This application takes an input, confirms it as int and is greater than a million. Otherwise, it'll loop and keep asking until the conditions are satisfied.

Part B: Calculates pi based on the Leibniz Formula with the index of 10, 1000, 100000, 500000, 1000000, and the value of part A's input (which is greater than a million). Printing the results on new lines.

Week 5 / Unit F
-------
Wrote our first Windows desktop application using C#. It calculates the monthly payment for a fixed interest rate loan, such as for a car or for a home mortgage.

Week 7
-------
Midterms week

Week 7
-------
Created a form that takes a secret word and hashes it using a phrase from the user, resulting into an encrypted phrase. This assignment uses 2D array and my own Playfair Cipher. 

Week 8
-------


Week 9
-------


Week 10
-------
This session was all about get, set, and including another class. A bank form was created asking the end-user for an account number, transaction amount, and if it was a deposit or withdrawal. The application calculated those transactions respectfully. There was a fee charged for overdrafts and limits on the amount that could be deposited. A status field gave an update on success, overdrafts, insufficient funds, or a deposit that was too big. A button when clicked, showed a transaction history.

Week 14
-------
For the past three weeks I was teamed with two other people to create an application that CRUD to an SQL database. The topic was movies and there was 6 inputs that were required with add, update, and delete.
Link: https://github.com/mystmn/CSCI-1630-FinalProject

Week 15
-------
For part 1 of the final, I had two days to create a calculator that read a file with thousands of lines and displayed the following: positive, negative, totals, min, max, and average numbers. Then write another file with only the positive numbers.
Link: https://github.com/mystmn/RainFallCalc

