﻿
namespace Excercise6
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelATMAmount = new System.Windows.Forms.Label();
            this.textBoxAtmTrans = new System.Windows.Forms.TextBox();
            this.buttonGetStatus = new System.Windows.Forms.Button();
            this.textBoxNetAmount = new System.Windows.Forms.TextBox();
            this.labelTotalAmount = new System.Windows.Forms.Label();
            this.textBoxStatusMessage = new System.Windows.Forms.TextBox();
            this.labelStatusMessage = new System.Windows.Forms.Label();
            this.radioButtonWithdraw = new System.Windows.Forms.RadioButton();
            this.radioButtonDeposit = new System.Windows.Forms.RadioButton();
            this.textBoxAccountNumber = new System.Windows.Forms.TextBox();
            this.labelAccountNumber = new System.Windows.Forms.Label();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonHistory = new System.Windows.Forms.Button();
            this.textBoxHistory = new System.Windows.Forms.TextBox();
            this.labelHistory = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelATMAmount
            // 
            this.labelATMAmount.AutoSize = true;
            this.labelATMAmount.Location = new System.Drawing.Point(61, 63);
            this.labelATMAmount.Name = "labelATMAmount";
            this.labelATMAmount.Size = new System.Drawing.Size(72, 13);
            this.labelATMAmount.TabIndex = 0;
            this.labelATMAmount.Text = "ATM Amount:";
            // 
            // textBoxAtmTrans
            // 
            this.textBoxAtmTrans.Location = new System.Drawing.Point(138, 59);
            this.textBoxAtmTrans.Name = "textBoxAtmTrans";
            this.textBoxAtmTrans.Size = new System.Drawing.Size(100, 20);
            this.textBoxAtmTrans.TabIndex = 2;
            // 
            // buttonGetStatus
            // 
            this.buttonGetStatus.Location = new System.Drawing.Point(138, 209);
            this.buttonGetStatus.Name = "buttonGetStatus";
            this.buttonGetStatus.Size = new System.Drawing.Size(75, 23);
            this.buttonGetStatus.TabIndex = 5;
            this.buttonGetStatus.Text = "Get Status";
            this.buttonGetStatus.UseVisualStyleBackColor = true;
            this.buttonGetStatus.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxNetAmount
            // 
            this.textBoxNetAmount.Location = new System.Drawing.Point(138, 85);
            this.textBoxNetAmount.Name = "textBoxNetAmount";
            this.textBoxNetAmount.ReadOnly = true;
            this.textBoxNetAmount.Size = new System.Drawing.Size(100, 20);
            this.textBoxNetAmount.TabIndex = 20;
            // 
            // labelTotalAmount
            // 
            this.labelTotalAmount.AutoSize = true;
            this.labelTotalAmount.Location = new System.Drawing.Point(62, 88);
            this.labelTotalAmount.Name = "labelTotalAmount";
            this.labelTotalAmount.Size = new System.Drawing.Size(73, 13);
            this.labelTotalAmount.TabIndex = 4;
            this.labelTotalAmount.Text = "Total Amount:";
            // 
            // textBoxStatusMessage
            // 
            this.textBoxStatusMessage.Location = new System.Drawing.Point(136, 134);
            this.textBoxStatusMessage.Multiline = true;
            this.textBoxStatusMessage.Name = "textBoxStatusMessage";
            this.textBoxStatusMessage.ReadOnly = true;
            this.textBoxStatusMessage.Size = new System.Drawing.Size(269, 58);
            this.textBoxStatusMessage.TabIndex = 21;
            // 
            // labelStatusMessage
            // 
            this.labelStatusMessage.AutoSize = true;
            this.labelStatusMessage.Location = new System.Drawing.Point(93, 137);
            this.labelStatusMessage.Name = "labelStatusMessage";
            this.labelStatusMessage.Size = new System.Drawing.Size(40, 13);
            this.labelStatusMessage.TabIndex = 6;
            this.labelStatusMessage.Text = "Status:";
            // 
            // radioButtonWithdraw
            // 
            this.radioButtonWithdraw.AutoSize = true;
            this.radioButtonWithdraw.Location = new System.Drawing.Point(288, 59);
            this.radioButtonWithdraw.Name = "radioButtonWithdraw";
            this.radioButtonWithdraw.Size = new System.Drawing.Size(70, 17);
            this.radioButtonWithdraw.TabIndex = 3;
            this.radioButtonWithdraw.TabStop = true;
            this.radioButtonWithdraw.Text = "Withdraw";
            this.radioButtonWithdraw.UseVisualStyleBackColor = true;
            // 
            // radioButtonDeposit
            // 
            this.radioButtonDeposit.AutoSize = true;
            this.radioButtonDeposit.Location = new System.Drawing.Point(288, 84);
            this.radioButtonDeposit.Name = "radioButtonDeposit";
            this.radioButtonDeposit.Size = new System.Drawing.Size(61, 17);
            this.radioButtonDeposit.TabIndex = 4;
            this.radioButtonDeposit.TabStop = true;
            this.radioButtonDeposit.Text = "Deposit";
            this.radioButtonDeposit.UseVisualStyleBackColor = true;
            // 
            // textBoxAccountNumber
            // 
            this.textBoxAccountNumber.Location = new System.Drawing.Point(138, 33);
            this.textBoxAccountNumber.Name = "textBoxAccountNumber";
            this.textBoxAccountNumber.ReadOnly = true;
            this.textBoxAccountNumber.Size = new System.Drawing.Size(100, 20);
            this.textBoxAccountNumber.TabIndex = 0;
            // 
            // labelAccountNumber
            // 
            this.labelAccountNumber.AutoSize = true;
            this.labelAccountNumber.Location = new System.Drawing.Point(40, 36);
            this.labelAccountNumber.Name = "labelAccountNumber";
            this.labelAccountNumber.Size = new System.Drawing.Size(90, 13);
            this.labelAccountNumber.TabIndex = 10;
            this.labelAccountNumber.Text = "Account Number:";
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(330, 209);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 7;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonHistory
            // 
            this.buttonHistory.Location = new System.Drawing.Point(233, 209);
            this.buttonHistory.Name = "buttonHistory";
            this.buttonHistory.Size = new System.Drawing.Size(75, 23);
            this.buttonHistory.TabIndex = 6;
            this.buttonHistory.Text = "Get History";
            this.buttonHistory.UseVisualStyleBackColor = true;
            this.buttonHistory.Click += new System.EventHandler(this.buttonHistory_Click);
            // 
            // textBoxHistory
            // 
            this.textBoxHistory.Location = new System.Drawing.Point(435, 59);
            this.textBoxHistory.Multiline = true;
            this.textBoxHistory.Name = "textBoxHistory";
            this.textBoxHistory.ReadOnly = true;
            this.textBoxHistory.Size = new System.Drawing.Size(195, 337);
            this.textBoxHistory.TabIndex = 13;
            // 
            // labelHistory
            // 
            this.labelHistory.AutoSize = true;
            this.labelHistory.Location = new System.Drawing.Point(512, 40);
            this.labelHistory.Name = "labelHistory";
            this.labelHistory.Size = new System.Drawing.Size(39, 13);
            this.labelHistory.TabIndex = 14;
            this.labelHistory.Text = "History";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(547, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Paul Cameron - 04/10/21";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelHistory);
            this.Controls.Add(this.textBoxHistory);
            this.Controls.Add(this.buttonHistory);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.labelAccountNumber);
            this.Controls.Add(this.textBoxAccountNumber);
            this.Controls.Add(this.radioButtonDeposit);
            this.Controls.Add(this.radioButtonWithdraw);
            this.Controls.Add(this.labelStatusMessage);
            this.Controls.Add(this.textBoxStatusMessage);
            this.Controls.Add(this.labelTotalAmount);
            this.Controls.Add(this.textBoxNetAmount);
            this.Controls.Add(this.buttonGetStatus);
            this.Controls.Add(this.textBoxAtmTrans);
            this.Controls.Add(this.labelATMAmount);
            this.Name = "Main";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelATMAmount;
        private System.Windows.Forms.TextBox textBoxAtmTrans;
        private System.Windows.Forms.Button buttonGetStatus;
        private System.Windows.Forms.TextBox textBoxNetAmount;
        private System.Windows.Forms.Label labelTotalAmount;
        private System.Windows.Forms.TextBox textBoxStatusMessage;
        private System.Windows.Forms.Label labelStatusMessage;
        private System.Windows.Forms.RadioButton radioButtonWithdraw;
        private System.Windows.Forms.RadioButton radioButtonDeposit;
        private System.Windows.Forms.TextBox textBoxAccountNumber;
        private System.Windows.Forms.Label labelAccountNumber;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonHistory;
        private System.Windows.Forms.TextBox textBoxHistory;
        private System.Windows.Forms.Label labelHistory;
        private System.Windows.Forms.Label label1;
    }
}

