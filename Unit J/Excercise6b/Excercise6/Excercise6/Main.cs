﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Excercise6
{
    public partial class Main : Form
    {
        BankAccount bank = new BankAccount(1362.59m, 000302019);

        public Main()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            decimal transactionAmount;
            if (textBoxAtmTrans.Text.Equals(""))
            {
                MessageBox.Show("Please enter a transaction amount");

                // Check if neither radio button is selected
            } else if(!decimal.TryParse(textBoxAtmTrans.Text, out transactionAmount)) {
                MessageBox.Show("Please enter a numeric value for your transaction amount");
            }
            else if (!radioButtonDeposit.Checked && !radioButtonWithdraw.Checked)
            {
                MessageBox.Show("Select withdraw or deposit");
            }
            else
            {
                string status = "";
                textBoxAccountNumber.Text = bank.VerifiedAccountNumber ? "000302019" : "0";

                // which radio button is used?
                if (radioButtonDeposit.Checked)
                {
                   status = bank.Deposit(transactionAmount);
                }

                if (radioButtonWithdraw.Checked)
                {
                    status = bank.Withdraw(transactionAmount);

                }

                // What was the status of the account and the amount transaction
                switch (status)
                {
                    //Okay, Overdrawn, InsufficientFunds, DepositTooLarge
                    case "Okay":
                        {
                            textBoxStatusMessage.Text = "Transaction Successful.";
                            break;
                        }
                    case "Overdrawn":
                        {
                            textBoxStatusMessage.Text = "Your account is overdrawn. Please make a deposit.";
                            break;
                        }
                    case "InsufficientFunds":
                        {
                            textBoxStatusMessage.Text = "Your account has insufficient funds for this transaction.";
                            break;
                        }
                    case "DepositTooLarge":
                        {
                            textBoxStatusMessage.Text = "The transaction amount is too large and cannot be submitted.";
                            break;
                        }
                }
                textBoxNetAmount.Text = Convert.ToString(bank.GetAccountBalance);
            }
        }
        // close the form when the button is clicked
        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonHistory_Click(object sender, EventArgs e)
        {
            // Reverse the order with the newest transactions on top
            bank.Ledgerhistory.Reverse();
            // Output all results from withdraw, deposits, and fees.
            textBoxHistory.Text = string.Join( Environment.NewLine, bank.Ledgerhistory);
        }
    }
}
