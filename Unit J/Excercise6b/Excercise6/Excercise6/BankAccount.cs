﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excercise6
{
    class BankAccount
    {

        // field
        private string status; //this.status
        public string Status { get { return status; } set { status = value; } }
        public enum GetAccountStatus { Okay, Overdrawn, InsufficientFunds, DepositTooLarge }

        private bool verifiedAccountNumber;
        public bool VerifiedAccountNumber { get { return verifiedAccountNumber; } }

        private decimal getaccountBalance;
        public decimal GetAccountBalance { get { return getaccountBalance; } set { getaccountBalance = value; } }

        //constructor
        // needs to pass the starting account balance and account number according to the requirements
        public BankAccount(decimal startingValue, int accountNumber)
        {
            getaccountBalance = startingValue;

            verifiedAccountNumber = accountNumber.Equals(000302019)? true : false;
        }

        private List<string> ledgerHistory = new List<string>();
        public List<string> Ledgerhistory { get { return ledgerHistory; } set { ledgerHistory = value; } }

        public string Deposit(decimal depositAmount)
        {
            // Check if the input is negative, throw an error
            if (depositAmount < 0)
            {
                return Convert.ToString(GetAccountStatus.InsufficientFunds);
            }
            // check if the deposit is greater than 10 thousand, throw an error
            else if (depositAmount > 10000m)
            {
                return Convert.ToString(GetAccountStatus.DepositTooLarge);
            }
            // Every other deposit is safe and will process
            else
            {
                this.getaccountBalance = this.getaccountBalance + depositAmount;
                ledgerHistory.Add(String.Format("+{0}", depositAmount));
                return Convert.ToString(GetAccountStatus.Okay);
            }
        }
        public string Withdraw(decimal withdrawAmount)
        {
            // if balance minus the withdraw is less than -100 (negative)
            // stop the withdraw and throw an error 
            if (this.getaccountBalance- withdrawAmount < -100m )
            {
                return Convert.ToString(GetAccountStatus.InsufficientFunds);

            }
            // If the balance minus the withdraw is less than zero and greater than or equal to -100
            // charge a fee and let the withdraw process
            else if (this.getaccountBalance- withdrawAmount < 0 && this.getaccountBalance-withdrawAmount >= -100)
            {
                decimal fee = 35.75m;
                this.getaccountBalance -= fee + withdrawAmount;
                ledgerHistory.Add(String.Format("-{0} (fee)", fee));
                ledgerHistory.Add(String.Format("-{0}", withdrawAmount));
                return Convert.ToString(GetAccountStatus.Overdrawn);
            }
            // Every withdraw greater than zero continues as normal
            else
            {
                this.getaccountBalance = this.getaccountBalance - withdrawAmount;
                ledgerHistory.Add(String.Format("-{0}", withdrawAmount));
                return Convert.ToString(GetAccountStatus.Okay);
            }
        }
    }
}
