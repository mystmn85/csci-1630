﻿using System;
// Creator: Paul Cameron
// Class: CSCI-1630-SP2021
// Date: 1/27/2021

/// <summary>
///     This program will input the height and width of the floor you'd like to install, 
///     multiple the time it takes to complete the job and output a calculated cost.
/// </summary> 

namespace Unit_A
{
    static class Constants
    {
        // Declaring the constants with their own class.
        public const string greeting = "Paul's Flooring Cost Estimator";
        public const Double costPerHour = 1.25;
        public const Double rateSqftPerMin = .008;
    }
    class Program
    {
        static void Main(string[] args)
        {
                 const string greeting = "Paul's Flooring Cost Estimator";
        	const Double costPerHour = 1.25;
        	const Double rateSqftPerMin = .008;
        	
            // Output a greeting
            Console.WriteLine(Constants.greeting);

            // Input the users floor length
            Console.WriteLine("Please enter the length of floor: ");
            Int32 length = Convert.ToInt32(Console.ReadLine());

            // Input the users floor width
            Console.WriteLine("Please enter the width of floor: "); 
            Int32 width = Convert.ToInt32(Console.ReadLine());

            // Input the cost per sqft of the flooring
            Console.WriteLine("Please enter the cost per square foot for the flooring selected: ");
            Double costPerSqFt = Convert.ToDouble(Console.ReadLine());

            // Calculate: Floor area, floor cost, time it will take to install, labor cost, and the invoice total to complete the job
            Int32 area = length * width;
            Double floorCost = area * costPerSqFt;
            Double installTime = area * Constants.rateSqftPerMin;
            Double laborCost = area * Constants.costPerHour;
            Double billingAmount = floorCost + laborCost;

            // Print the results for:
            // 
            // - floor area  / flooring cost 
            Console.WriteLine("For a total floor size of {0} the flooring cost is ${1}", area, floorCost.ToString("n2"));
            // - hours until completion / floor cost
            Console.WriteLine("It will take {0} hours to install the floor at a cost of ${1}", installTime.ToString("n2"), laborCost.ToString("n2"));
            // - invoice amount upon completion
            Console.WriteLine("The total finished cost for the new floor is: ${0}", billingAmount.ToString("n2"));

            // Pause the program and wait
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadLine();
        }
    }
}
